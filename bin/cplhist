#!/bin/bash
#
#     usage: cplhist_stats [options] cpl_hist_file [cpl_hist_file ...]
#   purpose: Create files containing min/max/avg of variables in coupler history files
#   options:
#     -l       ...list variables and meta data in each history file 
#     -v VLIST ...output specific variables from history files
#                 VLIST is a comma separated list of variable names
#                 Only variables in this list will appear in output files
#     -s       ...create 3 files containing min/max/avg over space
#     -t       ...create 3 files containing min/max/avg over time
#     -smin    ...create a file containing 1D time series of min over all lat/lon
#     -smax    ...create a file containing 1D time series of max over all lat/lon
#     -savg    ...create a file containing 1D time series of avg over all lat/lon
#     -tmin    ...create a file containing 2D grid of min over all time
#     -tmax    ...create a file containing 2D grid of max over all time
#     -tavg    ...create a file containing 2D grid of avg over all time
#     -diff    ...determine the difference between pairs of adjacent files found on
#                 the command line and then process these difference files rather
#                 than the user supplied history files
#
#   If no output specific command line options are supplied then only time average
#   files will be created.
#
#   Output files will have the same name as the input file but a suffix will be
#   added. The suffix will appear just before any existing ".nc" suffix and
#   it will be one of "_smin", "_smax", "_savg", "_tmin", "_tmax" or "_tavg".
#
###################################################################################
#
# Larry Solheim ...Jun,2016

Runame=$(basename $0)
FULLPATH=$(readlink -f $0)
usage() {
  [ -n "$1" ] && echo >&2 "$Runame" "$@"
  echo >&2 " "
  sed >&2 -n '/^###/q; s/^#$/# /; s/^ *$/# /; 3,$s/^# //p;' "$FULLPATH"
  exit
}

[ $# -eq 0 ] && usage

# Set defaults

# By default all variables except mask_atm and mask_ocn are included
# This default may be changed via the "-v" command line option
vars="-x -v mask_atm,mask_ocn"
user_set_vars=0

# List variables and meta data if the "-l" command line option is provided
list=0

# Process all ocean variables
ocnv=0

# Process all atm variables
atmv=0

# Determine the difference of adjacent files as they appear on the command line
diff_file=0

# Initialize output types
smin=0
smax=0
savg=0
tmin=0
tmax=0
tavg=0
user_set_out=0

# Initialize output control parameters
show_smin=0
show_smax=0
show_savg=0

# process command line options
while [[ $1 =~ ^- ]]; do
  opt=${1#-}
  case $opt in
    l) list=1; shift ;;
    s) smin=1; smax=1; savg=1; user_set_out=1; shift ;;
    t) tmin=1; tmax=1; tavg=1; user_set_out=1; shift ;;
    smin) smin=1; user_set_out=1; shift ;;
    smax) smax=1; user_set_out=1; shift ;;
    savg) savg=1; user_set_out=1; shift ;;
    show) show_smin=1; show_smax=1; show_savg=1; shift ;;
    tmin) tmin=1; user_set_out=1; shift ;;
    tmax) tmax=1; user_set_out=1; shift ;;
    tavg) tavg=1; user_set_out=1; shift ;;
    diff) diff_file=1; shift ;;
    v*) if [ -z "${opt#v}" ]; then
          # The next command line arg is the variable list
          shift
          [ -z "${1#-}" ] && { echo "Missing arg for -v"; exit 1; }
          vars="-v ${1#-}"
          shift
        else
          # The variable list is part of this command line arg
          vars="-v ${opt#v}"
          shift
        fi
        user_set_vars=1
       ;;
    *) echo "Unknown option -$opt"; exit 1 ;;
  esac
done

if [ $user_set_out -eq 0 ]; then
  # No specific output was requested, output only time average
  tavg=1
fi

if [ $user_set_vars -eq 1 ]; then
  # Always append lon_ocn,lat_ocn to the list of output variables unless the
  # default for vars is used (appending lon_ocn,lat_ocn to the default value
  # of vars would exclude these variables rather than include them)
  vars="${vars},lon_ocn,lat_ocn"
fi

[ $# -eq 0 ] && { echo "No file names were supplied on the command line."; usage; }

# All remaining command line args should be coupler history file names
filea=''
fileb=''
for cplhist in "$@"; do
  [ -z "$cplhist" ] && { echo "Empty command line arg"; exit 1; }
  [ -s "$cplhist" ] || { echo "File $cplhist is missing or empty"; exit 1; }

  # If a variable list is requested then list (ie ncdump) each file and exit
  if [ $list -eq 1 ]; then
    ncdump -h $cplhist
    continue
  fi

  if [ $diff_file -eq 1 ]; then
    # Read 2 files at a time and then difference them
    if [ -z "$filea" ]; then
      filea=$cplhist
      continue
    elif [ -z "$fileb" ]; then
      fileb=$cplhist
    fi

    cplhist=${filea%.nc}--${fileb%.nc}.nc
    ncdiff -O $vars $filea $fileb $cplhist
  fi
  # Reset these after reading every second command line arg
  filea=''
  fileb=''

  # echo "processing $cplhist"

  # Define output file names
  # Existing files by these names will be overwritten
  smin_cplhist=${cplhist%.nc}_smin.nc
  smax_cplhist=${cplhist%.nc}_smax.nc
  savg_cplhist=${cplhist%.nc}_savg.nc
  tmin_cplhist=${cplhist%.nc}_tmin.nc
  tmax_cplhist=${cplhist%.nc}_tmax.nc
  tavg_cplhist=${cplhist%.nc}_tavg.nc

  if [ $smin -eq 1 ]; then
    # Find the minimum value over lat/lon for each time element
    ncwa -O -I $vars -y min -a lat_a,lon_a,nlat_o,nlon_o $cplhist $smin_cplhist
    echo "Created $smin_cplhist"
  fi

  if [ $smax -eq 1 ]; then
    # Find the maximum value over lat/lon for each time element
    ncwa -O -I $vars -y max -a lat_a,lon_a,nlat_o,nlon_o $cplhist $smax_cplhist
    echo "Created $smax_cplhist"
  fi

  if [ $savg -eq 1 ]; then
    # Find the average value over lat/lon for each time element
    ncwa -O -I  $vars -a lat_a,lon_a,nlat_o,nlon_o $cplhist $savg_cplhist
    echo "Created $savg_cplhist"
    if [ $show_savg -eq 1 ]; then
      # Show the first 3 times for each spatially averaged variable
      ncks -H $savg_cplhist | sed -n '/time\[[012]\]/p'
    fi
  fi

  if [ $tmin -eq 1 ]; then
    # Find the minimum value over time for each time element
    ncwa -O -I $vars -y min -a time $cplhist $tmin_cplhist
    echo "Created $tmin_cplhist"
  fi

  if [ $tmax -eq 1 ]; then
    # Find the maximum value over time for each time element
    ncwa -O -I $vars -y max -a time $cplhist $tmax_cplhist
    echo "Created $tmax_cplhist"
  fi

  if [ $tavg -eq 1 ]; then
    # Find the average value over time for each time element
    ncwa -O -I $vars -a time $cplhist $tavg_cplhist
    echo "Created $tavg_cplhist"
  fi
done
