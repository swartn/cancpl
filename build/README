The compilation of CanCPL uses mkmf, which is a Makefile generator that understands fortran dependencies. 
The provided Makefile in this directory is used to call 'mkmf', and generate Makefiles for the following 
three libraries/executables:
    1. a CanDIAG library of routines that are used within CanCPL
    2. a CanAM library of routines that are used within CanCPL
    3. the main CanCPL executable
In addition to generating the new makefiles, the main Makefile, again calls 'make'

To add/remove routines to/from:
    - the CanDIAG library, add/remove the path to the source file to 'path_names_diag'
    - the CanAM library, add/remove the path to the source file to 'path_names_agcm'
    - the main coupler executable, add/remove the path to the source file to 'path_names_cplr'

To build the AGCM executable, the user must call 'make' with the following arguments provided:
    CPP_CONFIG_FILE=/PATH/TO/CPPFILE        -> cpp file containing various configuration settins
    CPP_SIZES_FILE=/PATH/TO/CPPFILE         -> cpp file containing various size parameters
    MKMF_TEMPLATE=/PATH/TO/MKMFTEMPLATE     -> template used by mkmf to generate the downstream Makefiles

When compiled as part of the production system, the cpp files are generated from user settings in 
various configuration files, but defaults are provided in 'include/.defaults' if users wish to do 
some test compilations. The mkmf template used in the production system is platform dependent, and can
be retrieved by following the PLATFORM link in this directory. THESE ARGUMENTS MUST BE EXPLICITLY GIVEN
TO AVOID SILENT COMPILATION ERRORS (i.e. using the wrong sizes files)

Note: to compile you will need to have the proper environment setup - to get this, source the 'compilation_environment'
      file within the associated PLATFORM directory.
